test_scripts = tests-default/gt_argument_tests.py       \
               tests-default/gt_download_tests.py       \
               tests-default/gt_inactivity_tests.py     \
               tests-default/gt_curl_ssl_verify_tests.py\
               tests-default/gt_script_tests.py         \
               tests-default/gt_cred_as_uri_tests.py

extra_test_scripts = tests-extra/gt_20gb_download_test.py   \
                     tests-extra/gt_upload_tests.py         \
                     tests-extra/gt_download_extra_tests.py

TESTS = run_tests.py


if DARWIN
LD_LIBRARY_PATH_name = DYLD_LIBRARY_PATH
else
LD_LIBRARY_PATH_name = LD_LIBRARY_PATH
endif

TESTS_ENVIRONMENT := PYTHONPATH="$(abs_top_builddir)/tests:$(abs_top_builddir)/scripts" \
                     PATH="$(abs_top_builddir)/tests:$(PATH)" \
                     $(LD_LIBRARY_PATH_name)="$($(LD_LIBRARY_PATH_name)):$(subst -L/,/,$(BOOST_LDFLAGS)):$(subst -L/,/,$(OPENSSL_LDFLAGS)):$(subst -L/,/,$(XQILLA_LDFLAGS))i:$(subst -L/,/,$(XERCES_LDFLAGS))"

EXTRA_DIST = $(test_scripts)              \
             README                       \
             mockhub.py                   \
             openssl-ca.cnf               \
             web.py-0.37.tar.gz           \
             utils/__init__.py            \
             utils/genetorrent.py         \
             utils/gttestcase.py          \
             utils/mockhubcontrol.py      \
             utils/cgdata/__init__.py     \
             utils/cgdata/analysis.py     \
             utils/cgdata/experiment.py   \
             utils/cgdata/datagen.py      \
             utils/cgdata/manifest.py     \
             utils/cgdata/run.py          \
             utils/config.py              \
             unittest2/case.py            \
             unittest2/collector.py       \
             unittest2/compatibility.py   \
             unittest2/__init__.py        \
             unittest2/loader.py          \
             unittest2/__main__.py        \
             unittest2/main.py            \
             unittest2/result.py          \
             unittest2/runner.py          \
             unittest2/signals.py         \
             unittest2/suite.py           \
             unittest2/util.py            \
             cgsubmit                     \
             run_tests.py                 \
             tests-default/__init__.py    \
             tests-extra/__init__.py      \
             $(test_scripts)              \
             $(extra_test_scripts)

certs:
	$(MKDIR_P) certs/
	@echo "Generating SSL CA certificate..."
	openssl req -new -x509 -days 3650 -extensions v3_ca \
		-nodes -config $(srcdir)/openssl-ca.cnf \
		-keyout certs/cakey.pem -out certs/cacert.pem \
		-extensions v3_ca

web:
	@echo "Unpacking web.py..."
	tar xzf $(srcdir)/web.py-0.37.tar.gz
	$(LN_S) web.py-0.37/web $@

# gtserver exec's gtoinfo so it needs to be there under that name ...
gtoinfo: $(srcdir)/../scripts/gtoinfo
	cp $< $@ ; chmod +x $@
	
# ... but some of the Python test code imports the gtoinfo module 
# so it needs to exist under the .py extension as well.
gtoinfo.py: gtoinfo
	cp $< $@
	
dhparam.pem: $(srcdir)/../src/dhparam.pem
	cp $< $@

cacert.pem: certs/cacert.pem certs
	cp $< $@

cgsubmit: $(srcdir)/../scripts/cgsubmit
	cp $< $@

.PHONY: check-setup
check-setup: web certs gtoinfo gtoinfo.py dhparam.pem cacert.pem cgsubmit

check-am: check-setup

CLEANFILES = dhparam.pem cacert.pem gtoinfo gtoinfo.py web cgsubmit

DISTCLEANFILES = gttest.log

clean-local:
	rm -rf web.py-*/
	rm -rf certs/
